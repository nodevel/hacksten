"""
Constants.

Copyright The Binary Trio.
"""

from typing import Any, Dict, List, Optional, Tuple


PDF_PATTERN = '*.pdf'
DIAGNOSES = ['I35.0', 'I35.2', 'I35.8', 'I35.9']

TRAIN = 0
VALIDATION = 1
TEST = 2
DATASETS = {
    TRAIN: 'train', VALIDATION: 'validation', TEST: 'test'
}

FEMALE = 1
MALE = 2
GENDERS = {
    'MALE': MALE,
    'FEMALE': FEMALE
}

# dataset creation
SIZE_SIDE = 300
PROMINENCE = 200
SIGNAL_FEATURE = 'V5'
SAMPLE_RATE = 500
COLUMNS = ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5',
           'V6']

# types
DatasetsType = Dict[int, List[Tuple]]
LabelType = Dict[str, Any]
LabelsType = Dict[str, LabelType]
LabelsTuple = Tuple[LabelsType, LabelsType, LabelsType]
