"""
Data splitting.
"""

import json
import logging
import os
from functools import partial
from multiprocessing import Pool
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

import numpy as np
import pandas as pd
import xmltodict
from imblearn.over_sampling import RandomOverSampler
from scipy.signal import find_peaks
from sklearn.model_selection import StratifiedShuffleSplit

from constants import (
    COLUMNS, DATASETS, DatasetsType, GENDERS, LabelType, LabelsType, LabelsTuple,
    PROMINENCE, SIGNAL_FEATURE, SIZE_SIDE, TEST, TRAIN, VALIDATION)
from id_tools import RC


LABEL_VALUES = [0, 1]


def sort_label(key: str, value: LabelType, val_keys: List[str],
               test_keys: List[str]):
    if key in val_keys:
        dataset = VALIDATION
    elif key in test_keys:
        dataset = TEST
    else:
        dataset = TRAIN

    return {**value, 'dataset': dataset}


def stratified_label_split(labels: LabelsType, val_size: float = 0.1,
                           test_size: float = 0.1, seed: Optional[int] = 42
                           ) -> LabelsTuple:
    keys = np.array(list(labels.keys()), dtype=object)
    flags = np.array([value['label']*1 for value in labels.values()])
    assert set(flags).issubset(LABEL_VALUES)
    sss_train = StratifiedShuffleSplit(
        test_size=(val_size + test_size), random_state=seed)
    for _, test_valid_index in sss_train.split(keys, flags):
        test_valid = keys[test_valid_index]
        test_valid_flags = flags[test_valid_index]

    split_size = (test_size / (val_size + test_size))
    sss_test = StratifiedShuffleSplit(test_size=split_size, random_state=seed)

    for test_index, valid_index in sss_test.split(test_valid, test_valid_flags):
        test_set = test_valid[test_index]
        valid_set = test_valid[valid_index]

    return {
        key: sort_label(key, value, valid_set, test_set)
        for key, value in labels.items()
    }


def id_from_filename(filename: str) -> str:
    name, _ = os.path.splitext(filename)
    first_part = name[:6]
    second_part = name[6:]
    return f'{first_part}/{second_part}'


def load_demographics(xml_path: str) -> Dict[str, Any]:
    with open(xml_path, 'r') as f:
        xml = xmltodict.parse(f.read())['RestingECG']
    demographics = xml['PatientDemographics']
    acq_year = pd.to_datetime(
        xml['TestDemographics']['AcquisitionDate']).year
    rc = RC.from_xml_id(demographics['PatientID'])
    age = rc.get_age(acq_year)
    if 'Gender' in demographics:
        gender = GENDERS[demographics['Gender']]
    else:
        gender = rc.gender
    return {'age': age, 'gender': gender, 'patient_id': rc.patient_id}


def load_labels(path: str) -> Dict[str, Dict[str, Any]]:
    with open(path, 'r') as f:
        labels = json.load(f)
    return labels


def load_ecg_info(csv_path: Path) -> Tuple[pd.DataFrame, Dict[str, int]]:
    data = pd.read_csv(csv_path)
    data.columns = [col.strip() for col in data.columns]
    assert set(COLUMNS).issubset(set(data.columns)), (
        f'{data.columns} {COLUMNS} {csv_path}')
    data = data[COLUMNS]
    xml_path = csv_path.with_suffix('.xml')
    demographics = load_demographics(xml_path)

    return data, demographics


def load_split_labels(labels_path: str, dir_path: str,
                      seed: Optional[int] = 42) -> Tuple[List, List]:
    labels = load_labels(labels_path)
    for path in Path(dir_path).rglob('*.xml'):
        patient_id = load_demographics(path)['patient_id']
        if patient_id not in labels:
            logging.info('Patient %s not found in labels', patient_id)
            labels[patient_id] = {'label': 0}

    return stratified_label_split(labels, seed=seed)


def load_single_ecg(path: Path, labels: LabelsType) -> List[Dict]:
    data, demographics = load_ecg_info(path)
    patient_id = demographics['patient_id']
    info = labels[patient_id]
    dataset = info['dataset']
    data_signal = data[SIGNAL_FEATURE]
    r_peaks, _ = find_peaks(data_signal, prominence=PROMINENCE, distance=SIZE_SIDE)
    r_valleys, _ = find_peaks(
        -data_signal, prominence=PROMINENCE, distance=SIZE_SIDE)

    # workaround for flipped signals
    if len(r_valleys) > len(r_peaks):
        r_peaks = r_valleys

    output = []
    for r_peak in r_peaks:
        start = r_peak - SIZE_SIDE
        end = r_peak + SIZE_SIDE
        if start < 0 or end > len(data):
            continue
        part = data.iloc[start:end, :].values
        assert len(part) == 2 * SIZE_SIDE
        output.append({
            'data': part, 'dataset': info['dataset'], 'label': info['label'],
            'gender': demographics['gender'], 'age': demographics['age']
        })

    return output


def load_data(dir_path: str, labels_path: str, output_dir: str,
              seed: Optional[int] = 42, oversample: bool = True
              ) -> Tuple[List, List]:
    labels = load_split_labels(labels_path, dir_path, seed)

    pool = Pool()
    func = partial(load_single_ecg, labels=labels)
    result = pool.map_async(func, Path(dir_path).rglob('*.csv'))

    data = [item for elem in result.get() for item in elem]

    datasets = {}
    for dataset in [TRAIN, VALIDATION, TEST]:
        path_stub = os.path.join(output_dir, DATASETS[dataset])
        dataset_items = [item for item in data if item['dataset'] == dataset]
        dataset_labels = [item['label'] for item in dataset_items]
        assert set(dataset_labels).issubset(set(LABEL_VALUES))
        if oversample:
            logging.info('Oversampling data')
            # workaround for imblearn
            dataset_items, dataset_labels = oversample_data(
                [[item] for item in dataset_items], dataset_labels, seed)
            dataset_items = [item[0] for item in dataset_items]
        dataset_data = np.array([item['data'] for item in dataset_items])
        np.savez_compressed(path_stub + '_data.npz', dataset_data)

        np.savez_compressed(path_stub + '_labels.npz', dataset_labels)

        dataset_demo = np.array(
            [[item['age'], item['gender']] for item in dataset_items])
        np.savez_compressed(path_stub + '_demo.npz', dataset_demo)

    return datasets


def oversample_data(data: List[Dict], labels: List[int],
                    seed: Optional[int] = 42) -> DatasetsType:
    oversample = RandomOverSampler(
        sampling_strategy='minority', random_state=seed)
    dataset_over, labels_over = oversample.fit_resample(data, labels)
    return dataset_over, labels_over
