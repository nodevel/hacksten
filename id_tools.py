"""
Patient ID tools
"""

import os

FEMALE = 1
MALE = 2


def clean_id(patient_id: str):
    first_part, second_part = patient_id.split('/')
    return f'{first_part[-6:]}/{second_part}'


class RC:
    def __init__(self, patient_id: str):
        self.patient_id = patient_id
        assert '/' in self.patient_id
        self.data, self.suffix = patient_id.split('/')

    @classmethod
    def from_xml_id(cls, text: str):
        if '/' in text:
            return cls(text)
        assert 9 <= len(text) <= 10
        first_part = text[:6]
        second_part = text[6:]
        return cls(f'{first_part}/{second_part}')

    @classmethod
    def from_filename(cls, filename: str):
        name, _ = os.path.splitext(filename)
        return cls.from_xml_id(name)

    @property
    def gender(self) -> int:
        month_part = int(self.data[2:4])
        if month_part < 50:
            return MALE

        return FEMALE

    @property
    def birth_year(self) -> int:
        year_part = int(self.data[:2])
        if len(self.suffix) == 3:
            century = 19
        elif year_part < 54:
            century = 20
        else:
            century = 19

        year = century * 100 + year_part

        return year

    def get_age(self, hospitalization_year: int) -> int:
        return hospitalization_year - self.birth_year
    
