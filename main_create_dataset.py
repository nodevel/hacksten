"""
Processing script for dataset creation.

Copyright The Binary Trio
"""

import argparse
import os
import logging

from data_tools import load_data



def main():
    parser = argparse.ArgumentParser(
        description='Create dataset from XML, CSV and labels.')
    parser.add_argument('--input-dir', type=str,
                        help='Input directory with XML and CSV ECG data')
    parser.add_argument('--labels', type=str,
                        help='Processed labels JSON')
    parser.add_argument('--seed', type=int, default=42,
                        help='Random seed')
    parser.add_argument('--output-dir', type=str, help='Output path')

    args = parser.parse_args()

    os.makedirs(args.output_dir, exist_ok=True)
    load_data(args.input_dir, args.labels, args.output_dir, args.seed)

if __name__ == '__main__':
    logging.basicConfig()
    main()
