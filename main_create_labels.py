"""
Data processing script for labels.

Copyright The Binary Trio
"""

import argparse
import json
import logging
from pathlib import Path
from typing import Any, Dict

import io
import regex
import pandas as pd
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams

from constants import DIAGNOSES, PDF_PATTERN
from id_tools import clean_id


LabelType = Dict[str, Dict[str, Any]]


START_TEXT = '\nDiagnózy\n'
END_TEXT = '\nPrůběh hospitalizace\n'


def pdf_path_to_diagnosis(path: str, limited_search: bool = False) -> bool:
    rsrcmgr = PDFResourceManager()
    retstr = io.StringIO()
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    # Process each page contained in the document.


    pattern = r'\b({})\b'.format('|'.join(map(regex.escape, DIAGNOSES)))
    patient_id = None
    with open(path, 'rb') as file_pointer:
        for i, page in enumerate(PDFPage.get_pages(file_pointer)):
            interpreter.process_page(page)
            data = retstr.getvalue()

            if limited_search:
                start_index = data.index(START_TEXT)
                end_index = data.index(END_TEXT)
                data = data[(start_index + len(START_TEXT)):end_index]
            lines = data.split('\n')
            # take values from the first page
            if i == 0:
                id_label_index = lines.index('Rodné číslo:')
                for line in lines[id_label_index:]:
                    if '/' in line:
                        patient_id = line
                        break
            search = set(map(str.lower, regex.findall(
                pattern, data, regex.IGNORECASE)))
            if search:
                return patient_id, True, search

    return patient_id, False, {}


def parse_pdfs(path: str) -> Dict[str, Dict[str, Any]]:
    results = {}

    for pdf_path in Path(path).rglob(PDF_PATTERN):
        patient_id, label, diagnoses = pdf_path_to_diagnosis(pdf_path)
        results[patient_id] = {
            'label': label,
            'diagnoses': ','.join(diagnoses)
        }
    return results


def parse_operation_labels(path: str) -> LabelType:
    data = pd.read_excel(path)

    keys = data.iloc[:, 0].dropna().map(clean_id)
    return {key: {'label': True} for key in keys}


def parse_diagnosis_csv(path: str) -> LabelType:
    data = pd.read_csv(path, index_col=0)

    labels = data['DgCode'].map(
        lambda x: any(x.startswith(diag) for diag in DIAGNOSES)).to_dict()
    return {key: {'label': value} for key, value in labels.items()}


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--input-pdf-in', type=str, default=None,
                        help='Input directory with PDFs from admissions')
    parser.add_argument('--input-pdf-out', type=str, default=None,
                        help='Input directory with PDFs from departures')
    parser.add_argument('--input-operation-xlsx', type=str, default=None,
                        help='Path to AS operation xlsx')
    parser.add_argument('--input-diagnosis-csv', type=str, default=None,
                        help='Path to diagnosis CSV')
    parser.add_argument('--output', type=str, help='Output json file path')

    args = parser.parse_args()
    assert args.output.endswith('.json')

    results = {}

    for path in [args.input_pdf_in, args.input_pdf_out]:
        if path:
            results = {**results, **parse_pdfs(path)}

    if args.input_diagnosis_csv:
        results = {**results, **parse_diagnosis_csv(args.input_diagnosis_csv)}

    if args.input_operation_xlsx:
        results = {
            **results, **parse_operation_labels(args.input_operation_xlsx)}

    with open(args.output, 'w') as file_pointer:
        json.dump(results, file_pointer)


if __name__ == '__main__':
    logging.basicConfig()
    main()
